USE [Nozama]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 7/4/2017 7:49:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[FirstLastName]  AS (([FirstName]+' ')+[LastName]),
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 7/4/2017 7:49:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[DateOrdered] [date] NOT NULL,
	[AmountPaid] [money] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 7/4/2017 7:49:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](1000) NOT NULL,
	[ProductNumber] [nvarchar](100) NULL,
	[Price] [money] NOT NULL,
	[Description] [nvarchar](4000) NULL,
	[InStock] [bit] NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 
GO
INSERT [dbo].[Customer] ([CustomerID], [FirstName], [LastName], [Address], [PhoneNumber]) VALUES (1, N'John', N'Jenkins', N'123 main street', NULL)
GO
INSERT [dbo].[Customer] ([CustomerID], [FirstName], [LastName], [Address], [PhoneNumber]) VALUES (2, N'Bill', N'Gilland', N'402 north', N'1234567891')
GO
INSERT [dbo].[Customer] ([CustomerID], [FirstName], [LastName], [Address], [PhoneNumber]) VALUES (3, N'Jake', N'Jimini', NULL, NULL)
GO
INSERT [dbo].[Customer] ([CustomerID], [FirstName], [LastName], [Address], [PhoneNumber]) VALUES (4, N'Laura', N'Fishengale', N'506 main', N'1232322323')
GO
INSERT [dbo].[Customer] ([CustomerID], [FirstName], [LastName], [Address], [PhoneNumber]) VALUES (5, N'Martin', N'Scrapelli', N'4342 South Intern', N'+821010232')
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 
GO
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [ProductID], [Quantity], [DateOrdered], [AmountPaid]) VALUES (1, 1, 3, 2, CAST(N'1980-01-01' AS Date), 50.0000)
GO
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [ProductID], [Quantity], [DateOrdered], [AmountPaid]) VALUES (2, 2, 3, 1, CAST(N'1990-01-01' AS Date), 30.0000)
GO
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [ProductID], [Quantity], [DateOrdered], [AmountPaid]) VALUES (3, 1, 1, 5, CAST(N'1990-10-01' AS Date), 200.0000)
GO
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [ProductID], [Quantity], [DateOrdered], [AmountPaid]) VALUES (4, 3, 4, 1, CAST(N'1990-01-10' AS Date), 100.0000)
GO
INSERT [dbo].[Orders] ([OrderID], [CustomerID], [ProductID], [Quantity], [DateOrdered], [AmountPaid]) VALUES (5, 4, 1, 8, CAST(N'1994-04-04' AS Date), 200.0000)
GO
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductNumber], [Price], [Description], [InStock], [Quantity]) VALUES (1, N'Wired Gaming Controller for PC, Playstation 3, Android, Steam', N'011', 39.9500, N'For PC Windows 10 & Play Station 3&Steam&Android (Above version 4.0,and the device must support the OTG function entirely)', 1, 500)
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductNumber], [Price], [Description], [InStock], [Quantity]) VALUES (2, N'GTX 1070', N'1234', 300.0000, N'1860 MHz Boost Clock with Super Alloy Power II Delivery', 1, 300)
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductNumber], [Price], [Description], [InStock], [Quantity]) VALUES (3, N'ASUS 4 GB RAM', N'222', 32.5000, N'One 4GB DDR RAM Chip', 1, 50)
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductNumber], [Price], [Description], [InStock], [Quantity]) VALUES (4, N'Hanns G 27" Monitor', N'1234', 200.0000, N'1920 x 1080 display', 1, 2000)
GO
INSERT [dbo].[Products] ([ProductID], [ProductName], [ProductNumber], [Price], [Description], [InStock], [Quantity]) VALUES (5, N'104 key keyboard', N'5454', 60.0000, N'Ergonomic black keyboard with backlighting', 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customer]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Products]
GO
