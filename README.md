Quick demonstration video showing project compiles and generates website: https://www.youtube.com/watch?v=f6zSEKEZWac&feature=youtu.be

A refresher project for practicing with html/css/javascript, SQL Server database, and ASP.NET services. Dummy customer ordering website.

For ASP.NET services, reference Nozama/*.
(Note: ASP.NET services uses modified patterns found in Shawn Wildermuth's tutorial: ASP.NET services with MVC 6, EFC, and AngularJS. )

For css/javascript -- reference: Nozama/wwwroot/*

For CSHTML -- reference: Nozama/Views/App/*

For SQL Database script, reference: backend/databaseScripts/*

