﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nozama.Services;
using Microsoft.Extensions.Configuration;
using Nozama.Models;
using Microsoft.EntityFrameworkCore;

namespace Nozama.Controllers.Web
{
    public class AppController : Controller
    {
        private IMailService _mailService;
        private IConfigurationRoot _config;
        private IWorldRepository _repository;

        public AppController(IMailService mailService, IConfigurationRoot config, IWorldRepository repository)
        {
            _mailService = mailService;
            _config = config;
            _repository = repository;
        }
        public IActionResult Index()
        {
            //Go to database and get all of the Products for display in index.cshtml.
            var data = _repository.GetAllProducts();
            
            return View(data);
        }

        public IActionResult Contact()
        {
            return View();
        }








        [HttpPost]
        public IActionResult Contact(ViewModels.ContactViewModel model)
        {

            if (model.Email.Contains("aol.com")) ModelState.AddModelError("Email", "We dont support AOL addresses");

            if (ModelState.IsValid)
            {
                _mailService.SendMail(_config["MailSettings:ToAddress"], model.Email, "From Nozama", model.Message);

                ModelState.Clear();
                ViewBag.UserMessage = "Message Sent";
            }

                return View();
        }

        public IActionResult About()
        {
            return View();
        }


    }


}
