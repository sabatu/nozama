﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;

namespace Nozama
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel() //Name of web server
                .UseContentRoot(Directory.GetCurrentDirectory()) //Identifies Content Location
                .UseIISIntegration() //Specialized headers, win authentication, etc.
                .UseStartup<Startup>() // Use class StartUp to set up web server
                .UseApplicationInsights()
                .Build();

            host.Run();
        }
    }
}
