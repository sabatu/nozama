﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Nozama.Models
{
    public class Products
    {
        
        public int ProductID { get; set; }
        public String ProductName { get; set; }
        public String ProductNumber { get; set; }
        public Decimal Price { get; set; }
        public String Description { get; set; }
        public Boolean InStock { get; set; }
        public int Quantity { get; set; }
        public byte[] Picture { get; set; }




    }
}


