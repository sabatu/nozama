﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nozama.Models
{
    public class Orders
    {
        
        public int OrderID { get; set; }
        
        public int CustomerID { get; set; }
        
        public int ProductID { get; set; }
        public int Quantity { get; set; }
        public DateTime DateOrdered { get; set; }
        public long  AmountPaid { get; set; }
 
    }
}
