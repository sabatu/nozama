﻿using System.Collections.Generic;

namespace Nozama.Models
{
    public interface IWorldRepository
    {
        IEnumerable<Products> GetAllProducts();
    }
}