﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace Nozama.Models
{
    public class WorldContext : DbContext
    {
        private IConfigurationRoot _config;

        public WorldContext(Microsoft.Extensions.Configuration.IConfigurationRoot config, DbContextOptions options) : base()
        {
            _config = config;
        }
        protected override void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasKey(c => c.CustomerId);

            modelBuilder.Entity<Orders>()
            .HasKey(c => c.CustomerID);

            modelBuilder.Entity<Orders>()
           .HasKey(c => c.OrderID);

            modelBuilder.Entity<Orders>()
            .HasKey(c => c.ProductID);

            modelBuilder.Entity<Products>()
            .HasKey(c => c.ProductID);


        }



        public DbSet<Products> Products { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Customer> Customer { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlServer(_config["ConnectionStrings:WorldContextConnection"]);


        }


    }
}
